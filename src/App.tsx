import { useHover } from './hooks/useHover'

export const App = () => {
  const { hovered, ref } = useHover()

  return (
    <div className="flex content-center p-4 justify-center">
      <div ref={ref} className="bg-orange-300 text-xl p-2 rounded min-w-64">
        {hovered ? 'На меня навели мышку' : 'Наведи мышкой на меня'}
      </div>
    </div>
  )
}
