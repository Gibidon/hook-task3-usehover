import { useEffect, useRef, useState } from 'react'

export function useHover() {
  const [hovered, setHovered] = useState<boolean>(false)
  const ref = useRef<HTMLDivElement>(null)

  useEffect(() => {
    function MouseOverHandler(e: MouseEvent) {
      if (ref.current) {
        if (ref.current.contains(e.target as Node)) {
          // is it ok?
          setHovered(true)
        } else setHovered(false)
      }
    }

    window.addEventListener('mouseover', MouseOverHandler)

    return () => window.removeEventListener('mouseover', MouseOverHandler)
  }, [])

  return { hovered, ref }
}
